(function() {
  angular.module('app', ['ui.router', 'smart-table'])
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });

      $stateProvider
        .state('place-new', {
          url: '/places/new',
          templateUrl: '/templates/places/create-edit.html',
          controller: 'PlaceNewCtrl'
        })
        .state('place-edit', {
          url: '/places/:id/edit',
          templateUrl: '/templates/places/create-edit.html',
          controller: 'PlaceEditCtrl'
        })
        .state('place-show', {
          url: '/places/:id',
          templateUrl: '/templates/places/show.html',
          controller: 'PlaceShowCtrl'
        })
        .state('place-list', {
          url: '/places',
          templateUrl: '/templates/places/index.html',
          controller: 'PlaceListCtrl',
          controllerAs: 'listCtrl'
        });



        $urlRouterProvider.otherwise('/places');

    }])

    .controller('PlaceListCtrl', ['$scope', '$http',
      function($scope, $http) {
console.log('list');
        var listCtrl = this;

        $http.get('/api/v1/places').then(function(resp) {
            listCtrl.places = resp.data;
        });

        $scope.remove = function(place) {

          if (confirm('Are you sure you want to delete '+ place.name +'?')) {
            $http.delete('/api/v1/places/'+ place.id).then(
              function() {
                listCtrl.places.find(function(p, i) {
                  if (p === place) {
                    listCtrl.places.splice(i, 1);
                  }
                });
              }
            );
          }

        };

      }
    ])

    .controller('PlaceEditCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        $http.get('/api/v1/places/'+ $stateParams.id).then(function(resp) {
          var place = resp.data;

          $scope.place = {
            name: place.name,
            address: place.address,
            city: place.city,
            notes: place.notes
          };
        });

        $scope.submit = function() {
          $http.put('/api/v1/places/'+ $stateParams.id, $scope.place).then(
            function() {
              $state.go('place-list');
            }
          )
        };

      }
    ])
    .controller('PlaceShowCtrl', ['$scope', '$http', '$stateParams',
      function($scope, $http, $stateParams) {

        $http.get('/api/v1/places/'+ $stateParams.id).then(function(resp) {
          $scope.place = resp.data;
        });

      }
    ])
    .controller('PlaceNewCtrl', ['$scope', '$http', '$stateParams', '$state',
      function($scope, $http, $stateParams, $state) {

        $scope.place = {};

        $scope.submit = function() {
          $http.post('/api/v1/places', $scope.place).then(
            function() {
              $state.go('place-list');
            }
          )
        };

      }
    ])

})();
