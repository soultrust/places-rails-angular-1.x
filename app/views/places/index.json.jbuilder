json.array!(@places) do |place|
  json.extract! place, :id, :name, :address, :website, :city, :notes
  json.url place_url(place, format: :json)
end
