# Graphical interface for my places database #

My places database is comprised mostly of restaurants and stores. I built this GUI to keep track of places I've been. My own personal yelp.com. 

Built in Angular 1.4 and Rails. I have stopped development on this project since I have decided to recreate it in Angular 2 in a new repo.